# Use systemd image ubuntu 18.04(bionic)
FROM solita/ubuntu-systemd:bionic

RUN apt-get update && apt-get install -y software-properties-common sudo
RUN useradd -m -s /bin/bash ubuntu && usermod -aG sudo ubuntu
RUN echo "ubuntu ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers